package com.iesinfantaelena.ejg9_brahyanmarin;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnIniciar, btnPausar, btnDetener, btnNoBucle, btnContinuar;

    MediaPlayer reproductor;

    boolean reproducirEnBucle = true;
    int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iniciar();
    }

    @Override
    public void onClick(View view) {

        if (view == btnIniciar)
            iniciarMusica();

        if(view == btnPausar)
            pausarMusica();

        if(view == btnContinuar)
            continuarMusica();

        if(view == btnDetener)
            detenerMusica();

        if(view == btnNoBucle)
            noBucle();
    }

    private void noBucle(){
        if (reproducirEnBucle){
            reproducirEnBucle = false;
            btnNoBucle.setText("REPRODUCIR EN BUCLE ");
        }else {
            reproducirEnBucle = true;
            btnNoBucle.setText("NO REPRODUCIR EN BUCLE");
        }

        if (reproductor != null)
            reproductor.setLooping(reproducirEnBucle);
    }

    private void detenerMusica() {
        if(reproductor != null){
            position = 0;
            reproductor.stop();
            liberarMediaPlayer();
        }
    }

    private void continuarMusica(){
        if(reproductor != null && !reproductor.isPlaying()){
            reproductor.seekTo(position);
            reproductor.start();
        }
    }

    private void pausarMusica(){
        if(reproductor != null && reproductor.isPlaying()){
           position = reproductor.getCurrentPosition();
           reproductor.pause();
        }
    }

    private void iniciarMusica(){
        position = 0;
        liberarMediaPlayer();
        reproductor = MediaPlayer.create(this,R.raw.musica);
        reproductor.setLooping(reproducirEnBucle);
        reproductor.start();
    }

    private void liberarMediaPlayer(){
        if(reproductor != null){
            reproductor.release();
            reproductor = null;
        }
    }

    private void iniciar(){

        btnDetener = findViewById(R.id.btnDetener);
        btnIniciar = findViewById(R.id.btnIniciar);
        btnPausar = findViewById(R.id.btnPausar);
        btnNoBucle = findViewById(R.id.btnNoBucle);
        btnContinuar = findViewById(R.id.btnContinuar);

        btnDetener.setOnClickListener(this);
        btnIniciar.setOnClickListener(this);
        btnPausar.setOnClickListener(this);
        btnNoBucle.setOnClickListener(this);
        btnContinuar.setOnClickListener(this);
    }
}